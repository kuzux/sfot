#pragma once

#include "pch.h"

#include "operation.h"
#include "addressing_mode.h"

class RAM {
public:
    uint8_t read_byte(uint16_t addr) const;
    uint16_t read_word(uint16_t addr) const;

    void write(uint16_t addr, uint8_t byte);

    RAM(size_t size);
    void load_image(const uint8_t* bin, size_t bytes);
    size_t size() const;

    // returns start_addr, [bytes] start_addr might be slightly behind the given addr
    std::tuple<uint16_t, std::vector<uint8_t>> look_around(uint16_t addr) const;

    // TODO: Also support returning a range
private:
    std::vector<uint8_t> bytes;
};

// instruction set info from https://www.masswerk.at/6502/6502_instruction_set.html
class Instruction {
public:
    static Instruction from_opcode(uint8_t opcode);

    Operation operation() const;
    AddressingMode addressing_mode() const;
    uint8_t opcode() const { return m_opcode; }
    
    // cycles returns the base case (a page boundary hasn't been crossed)
    uint8_t cycles() const;
    uint8_t length() const;
    std::string_view mnemonic() const;
private:
    Instruction(uint8_t opcode) : m_opcode(opcode) { }
    uint8_t m_opcode;
};

class CPU {
public:
    struct Status {
        union {
            uint8_t value;
            struct {
                uint8_t carry : 1;              // C
                uint8_t zero : 1;               // Z
                uint8_t disable_interrupts : 1; // I
                uint8_t decimal_mode : 1;       // D
                uint8_t break_ : 1;             // B
                uint8_t _ignored : 1;
                uint8_t overflow : 1;           // V
                uint8_t negative : 1;           // N
            };
            struct {
                uint8_t C : 1; // C
                uint8_t Z : 1; // Z
                uint8_t I : 1; // I
                uint8_t D : 1; // D
                uint8_t B : 1; // B
                uint8_t _i : 1;
                uint8_t V : 1; // V
                uint8_t N : 1; // N
            };
        };
    };

    bool running() const { return !halted; }
    void reset();
    // TODO: Some way to attach memory (with memory mapped io etc). The current ctor is a temporary solution
    CPU(RAM& ram) : memory(ram) { }

    // returns the actual number of cycles the cpu ran for (might be slightly higher/lower than target)
    int execute(int target_cycles);

    Status SR;
    uint16_t PC;
    uint8_t A, X, Y;
    uint8_t SP;

    // methods for use in debugger
    std::unordered_map<std::string, std::string> registers_info() const;
    // does not consume any clock cycles
    void skip_next();
    void add_breakpoint(uint16_t addr);

    int step(int num_steps);
    int cont(); // continue

    void pause_execution();

private:
    bool halted = false;
    bool debug_interrupted = false;

    // Memory should be able to respond to any given address
    // TODO: This is obviously a temporary thing, we need a memory map/controller (with ROM, RAM, IO etc)
    RAM& memory;

    std::unordered_set<uint16_t> breakpoints;

    Instruction fetch_opcode(uint16_t addr);

    // returns (operand, setter, number of extra cycles), takes the address of the byte after the opcode
    // takes both of the possible arguments to the operation. only one is used depending on the addressing mode
    std::tuple<uint8_t, std::function<void(uint8_t)>, int> fetch_operand(AddressingMode mode, uint8_t byte_arg, uint16_t word_arg);

    // returns jump addr (used by jump instructions) takes the address of the byte after the opcode
    // extra cycles are handled by the fetch_operand method
    uint16_t fetch_jump_address(AddressingMode addr, uint8_t byte_arg, uint16_t word_arg);

    // returns number of cycles executed
    int execute_next();

    // sets Z and N flags
    void set_value_flags(uint8_t value);
};
