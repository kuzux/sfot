#pragma once 

#include <string>
#include <array>
#include <stdint.h>

#include "operation.h"
#include "addressing_mode.h"

struct OpcodeInfo {
    std::string mnemonic;
    Operation op;
    AddressingMode am;
    uint8_t cycles;
};

extern std::array<OpcodeInfo, 256> opcodes;