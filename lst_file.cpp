#include "pch.h"

#include "lst_file.h"
using namespace std;

LstFile::LstFile(std::string filename) {
    ifstream in(filename);
    if(!in.good()) return;

    vector<string> lines_in_file;
    int header_end = -1;
    int line_num = 0;
    while(!in.eof()) {
        string curr_line;
        getline(in, curr_line);
        if(header_end != -1) {
            if(curr_line == "") continue;
            uint16_t addr;
            from_chars(curr_line.data(), curr_line.data()+6, addr, 16);
            string code_line = curr_line.substr(24);
            bool reloc = curr_line[6] == 'r';
            m_lines.push_back(LstLine {addr, code_line, reloc});
        }
        if(header_end == -1 && curr_line=="") {
            header_end = line_num;
        }

        line_num++;
    }

    if(header_end != -1) {
        m_loaded = true;
    }

    in.close();
}

std::vector<std::tuple<uint16_t, std::string>> LstFile::get_listing(uint16_t addr) const {
    int first = -1;
    int last = -1;

    // assume the listing file is sorted by address
    // TODO: Make sure that is true

    for(size_t i=0; i < m_lines.size(); i++) {
        const LstLine& line = m_lines[i];
        // we do not care about relocatable lines
        if(line.relocatable) continue;

        if(line.address == addr) {
            // exact match
            if(first == -1) first = i;
            last = i;
            continue;
        }

        if(line.address > addr) {
            // inexact match, set the previous line as the first occurence and this one as the last one
            // and make sure we do not modify the last occurrence later
            first = i - 1;
            last = i;
            break;
        }
    }

    std::vector<std::tuple<uint16_t, std::string>> res;
    if(first == -1) return res;

    int start = first - 5;
    size_t end = last + 10;

    if(start < 0) start = 0;
    if(end > m_lines.size()) end = m_lines.size();

    for(size_t i=(size_t)start; i<(size_t)end; i++) res.push_back({ m_lines[i].address, m_lines[i].line });
    return res;
}