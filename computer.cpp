#include "pch.h"
#include "computer.h"
#include "opcodes.h"

#include <assert.h>

Instruction Instruction::from_opcode(uint8_t opcode) {
    Instruction res { opcode };
    return res;
}

AddressingMode Instruction::addressing_mode() const {
    return opcodes[m_opcode].am;
}

Operation Instruction::operation() const {
    return opcodes[m_opcode].op;
}

uint8_t Instruction::cycles() const {
    return opcodes[m_opcode].cycles;
}

uint8_t Instruction::length() const {
    // length of the instruction (opcode + operand)
    uint8_t res = 1; // for opcode
    switch(addressing_mode()) {
        case AddressingMode::Absolute:
        case AddressingMode::AbsoluteX:
        case AddressingMode::AbsoluteY:
        case AddressingMode::Indirect:
        res += 2;
        break;

        case AddressingMode::Immediate:
        case AddressingMode::ZeroPage:
        case AddressingMode::ZeroPageX:
        case AddressingMode::ZeroPageY:
        case AddressingMode::Relative:
        // kind of odd that indirect addressing mode is 3 bytes in length
        // but x-ind and ind-y are only 2
        case AddressingMode::XIndirect:
        case AddressingMode::IndirectY:

        res += 1;
        break;

        case AddressingMode::Implied:
        case AddressingMode::Accumulator:

        break;
    }

    return res;
}

std::string_view Instruction::mnemonic() const {
    return opcodes[m_opcode].mnemonic;
}

RAM::RAM(size_t size) {
    bytes.resize(size); // they'll be zero initialized
}

uint8_t RAM::read_byte(uint16_t addr) const {
    if(addr > bytes.size()) return 0xFF;

    return bytes[addr];
}

uint16_t RAM::read_word(uint16_t addr) const {
    uint8_t lo = read_byte(addr);
    uint8_t hi = read_byte(addr+1);

    return (hi << 8) | lo;
}

void RAM::write(uint16_t addr, uint8_t value) {
    if(addr > bytes.size()) return;
    bytes[addr] = value;
}

size_t RAM::size() const {
    return bytes.size();
}

Instruction CPU::fetch_opcode(uint16_t addr) {
    return Instruction::from_opcode(memory.read_byte(addr));
}

std::tuple<uint8_t, std::function<void(uint8_t)>, int> CPU::fetch_operand(AddressingMode mode, uint8_t byte_arg, uint16_t word_arg) {
    uint8_t value = 0x0;
    std::function<void(uint8_t)> setter = [](uint8_t _arg){ (void)_arg; assert(false); };
    int extra_cycles = 0;

    switch(mode) {
        case AddressingMode::Implied:
        break;

        case AddressingMode::Immediate:
        value = byte_arg;
        break;

        case AddressingMode::Accumulator:
        value = A;
        setter = [this](uint8_t arg){ A = arg; };
        break;

        case AddressingMode::Absolute: {
            uint16_t effective_address = word_arg;
            value = memory.read_byte(effective_address);
            setter = [this, effective_address](uint8_t arg){ memory.write(effective_address, arg); };
        } break;

        case AddressingMode::AbsoluteX: {
            uint16_t without_offset = word_arg;
            uint16_t with_offset = without_offset + X;

            value = memory.read_byte(with_offset);
            setter = [this, with_offset](uint8_t arg){ memory.write(with_offset, arg); };

            if((without_offset & 0xFF00) != (with_offset & 0xFF00)) extra_cycles = 1;
        } break;

        case AddressingMode::AbsoluteY: {
            uint16_t without_offset = word_arg;
            uint16_t with_offset = without_offset + Y;

            value = memory.read_byte(with_offset);
            setter = [this, with_offset](uint8_t arg){ memory.write(with_offset, arg); };

            if((without_offset & 0xFF00) != (with_offset & 0xFF00)) extra_cycles = 1;
        } break;

        case AddressingMode::Relative: {
            int without_offset = PC+2;
            uint8_t delta = byte_arg;
            int with_offset = without_offset + static_cast<int8_t>(delta);

            // this instruction is only used for jump instructions
            // so we don't even need to reeturn getters/setters for this

            // returning this is required though
            if((without_offset & 0xFF00) != (with_offset & 0xFF00)) extra_cycles = 2;
        } break;

        case AddressingMode::Indirect: {
            uint16_t addr = word_arg;
            uint16_t effective_address = memory.read_word(addr);

            value = memory.read_byte(effective_address);
            setter = [this, effective_address](uint8_t arg){ memory.write(effective_address, arg); };
        } break;

        case AddressingMode::XIndirect: {
            uint8_t addr = static_cast<uint16_t>(byte_arg + X);
            uint16_t effective_address = memory.read_word(addr);

            value = memory.read_byte(effective_address);
            setter = [this, effective_address](uint8_t arg){ memory.write(effective_address, arg); };
        } break;

        case AddressingMode::IndirectY: {
            uint16_t addr = static_cast<uint16_t>(byte_arg);
            uint16_t without_offset = memory.read_word(addr);
            uint16_t with_offset = without_offset + Y;

            value = memory.read_byte(with_offset);
            setter = [this, with_offset](uint8_t arg){ memory.write(with_offset, arg); };

            if((without_offset & 0xFF00) != (with_offset & 0xFF00)) extra_cycles = 1;
        } break;

        case AddressingMode::ZeroPage: {
            uint16_t effective_address = static_cast<uint16_t>(byte_arg);
            value = memory.read_byte(effective_address);
            setter = [this, effective_address](uint8_t arg){ memory.write(effective_address, arg); };
        } break;

        case AddressingMode::ZeroPageX: {
            uint8_t zp_addr = byte_arg;
            zp_addr += X;
            uint16_t effective_address = static_cast<uint16_t>(zp_addr);
            value = memory.read_byte(effective_address);
            setter = [this, effective_address](uint8_t arg){ memory.write(effective_address, arg); };
        } break;

        case AddressingMode::ZeroPageY: {
            uint8_t zp_addr = byte_arg;
            zp_addr += Y;
            uint16_t effective_address = static_cast<uint16_t>(zp_addr);
            value = memory.read_byte(effective_address);
            setter = [this, effective_address](uint8_t arg){ memory.write(effective_address, arg); };
        } break;
    }

    return std::make_tuple(value, setter, extra_cycles);
}

uint16_t CPU::fetch_jump_address(AddressingMode mode, uint8_t byte_arg, uint16_t word_arg) {
    uint16_t effective_addr = 0x00;
    switch(mode) {
        case AddressingMode::Implied:
        case AddressingMode::Immediate:
        case AddressingMode::Accumulator:
        // these do not make sense for returning jump addresses
        return 0x00;

        case AddressingMode::ZeroPage:
        case AddressingMode::ZeroPageX:
        case AddressingMode::ZeroPageY:
        case AddressingMode::AbsoluteX:
        case AddressingMode::AbsoluteY:
        case AddressingMode::XIndirect:
        case AddressingMode::IndirectY:
        // these are not used for jump instructions
        return 0x00;

        case AddressingMode::Absolute:
        effective_addr = word_arg;
        break;

        case AddressingMode::Indirect: {
            uint16_t addr = word_arg;
            effective_addr = memory.read_word(addr);
        } break;

        case AddressingMode::Relative: {
            int without_offset = PC;
            uint8_t delta = byte_arg;
            int with_offset = without_offset + static_cast<int8_t>(delta);

            effective_addr = (uint16_t)with_offset;
        } break;
    }

    return effective_addr;
}

void CPU::set_value_flags(uint8_t value) {
    SR.zero = (value == 0);
    SR.negative = (value & 1<<7) != 0;
}

void CPU::reset() {
    SR = Status { 0x00 };
    A = X = Y = 0x00;
    SP = 0xFF;
    halted = false;
    PC = memory.read_word(0xFFFC);
}

int CPU::execute_next() {
    Instruction instr = fetch_opcode(PC);
    uint8_t cycles = instr.cycles();

    uint8_t byte_arg = memory.read_byte(PC+1);
    uint16_t word_arg = memory.read_word(PC+1);

    // We should increment PC after the instruction is fetched
    PC += instr.length();

    // after fetching the arguments to the instruction, we need to fetch the actual operands to it
    auto [operand, set_operand, extra_cycles] = fetch_operand(instr.addressing_mode(), byte_arg, word_arg);
    // only valid/used for jump instructions, no need for set
    uint16_t jump_addr = fetch_jump_address(instr.addressing_mode(), byte_arg, word_arg);
    Operation op = instr.operation();

    // takes a, b, c => returns result, overflow, carry
    auto do_add = [](uint8_t a, uint8_t b, bool carry) -> std::tuple<uint8_t, bool, bool> {
        // http://www.righto.com/2013/01/a-small-part-of-6502-chip-explained.html

        uint8_t c = carry ? 1 : 0;

        uint8_t mask06 = 0x7f;
        uint8_t mask7 = 0x80;

        // calculating first 6 bytes to get a c6 bit
        uint8_t a06 = a & mask06;
        uint8_t b06 = b & mask06;

        uint8_t res06 = a06 + b06 + c;
        bool c6 = (res06 & mask7) != 0;

        uint16_t res16 = (uint16_t)a + (uint16_t)b + (uint16_t)c;
        uint8_t res = a + b + c;
        bool c7 = (res16 & 0xff00) != 0;

        bool a7 = (a & mask7) != 0;
        bool b7 = (b & mask7) != 0;

#define nor(x, y) (!(x||y))
#define nand(x, y) (!(x&&y))
        bool nor7 = nor(a7, b7);
        bool nand7 = nand(a7, b7);

        bool int1 = c6 & nor7;
        bool int2 = nor(nand7, c6);

        bool v = !nor(int1, int2);
#undef nor
#undef nand

        return { res, v, c7 };
    };

    auto hex_to_bcd = [](uint8_t hex) -> uint8_t {
        uint8_t tens = (hex & 0xf0) >> 4;
        uint8_t units = (hex & 0x0f);

        if(units >= 10) {
            tens++;
            units -= 10;
        }
        if(tens >= 10) tens -= 10;

        return 10 * tens + units;
    };

    auto bcd_to_hex = [](uint8_t bcd) {
        bcd %= 100;

        uint8_t tens = bcd / 10;
        uint8_t units = bcd % 10;

        return (tens << 4) | units;
    };

    // some operations that take a long time (and invalid ones) do not cause extra clock cycles
    if(op != Operation::Invalid && op == Operation::ShiftLeft && op != Operation::ShiftRight
        && op != Operation::RotateLeft && op != Operation::RotateRight) cycles += extra_cycles;

    switch(op) {
        case Operation::Halt:
        halted = true;
        break;

        // Load/Store

        case Operation::LoadAcc:
        A = operand;
        set_value_flags(A);
        break;
        case Operation::LoadX:
        X = operand;
        set_value_flags(X);
        break;
        case Operation::LoadY:
        Y = operand;
        set_value_flags(Y);
        break;

        case Operation::StoreAcc:
        set_operand(A);
        break;
        case Operation::StoreX:
        set_operand(X);
        break;
        case Operation::StoreY:
        set_operand(Y);
        break;

        // Transfers

        case Operation::AccToX:
        X = A;
        set_value_flags(X);
        break;
        case Operation::AccToY:
        Y = A;
        set_value_flags(Y);
        break;
        case Operation::XToAcc:
        A = X;
        set_value_flags(A);
        break;
        case Operation::YToAcc:
        A = Y;
        set_value_flags(A);
        break;

        case Operation::SPToX:
        X = SP;
        set_value_flags(X);
        break;
        case Operation::XToSP:
        SP = X;
        // this instruction does not modify the flags register
        break;

        // Stack

        case Operation::PushAcc: {
            uint16_t addr = 0x1 << 8 | SP;
            memory.write(addr, A);
            SP--;
        } break;
        case Operation::PullAcc: {
            SP++;
            uint16_t addr = 0x1 << 8 | SP;
            A = memory.read_byte(addr);
            set_value_flags(A);
        } break;

        case Operation::PushStatus: {
            uint16_t addr = 0x1 << 8 | SP;
            Status copy { SR.value };
            copy.break_ = 1;
            copy._ignored = 1; // this is also set while pushing
            memory.write(addr, copy.value);
            SP--;
        } break;
        case Operation::PullStatus: {
            SP++;
            uint16_t addr = 0x1 << 8 | SP;
            uint8_t new_status = memory.read_byte(addr);
            Status copy { new_status };
            copy.break_ = 0;
            copy._ignored = 0;
            SR.value = copy.value;
        } break;

        // Arithmetic
        // TODO: Handle decimal mode (is it also used in inc/dec instructions?)

        case Operation::Decrement:
        set_operand(--operand);
        set_value_flags(operand);
        break;
        case Operation::Increment:
        set_operand(++operand);
        set_value_flags(operand);
        break;

        case Operation::DecrementX:
        X--;
        set_value_flags(X);
        break;
        case Operation::IncrementX:
        X++;
        set_value_flags(X);
        break;

        case Operation::DecrementY:
        Y--;
        set_value_flags(Y);
        break;
        case Operation::IncrementY:
        Y++;
        set_value_flags(Y);
        break;

        case Operation::Add: 
        if(SR.D) {
            uint8_t A_dec = hex_to_bcd(A);
            uint8_t M_dec = hex_to_bcd(operand);

            uint8_t R_dec = A_dec + M_dec + SR.C;

            if(R_dec >= 100) SR.C = 1;
            else SR.C = 0;

            A = bcd_to_hex(R_dec);
            set_value_flags(A);
        } else {
            auto [result, v, c] = do_add(A, operand, SR.C);

            A = result;
            set_value_flags(A);
            SR.C = c;
            SR.V = v;
            // fmt::print("flags 0x{:02x}\n", SR.value);
        } break;
        case Operation::Subtract: 
        if(SR.D) {
            uint8_t A_dec = hex_to_bcd(A);
            uint8_t M_dec = hex_to_bcd(operand);

            uint8_t B = 1 - SR.C;

            uint8_t R_dec;
            if(M_dec + B > A_dec) {
                R_dec = 100 + A_dec - (M_dec + B);
                SR.C = 0;
            } else {
                R_dec = A_dec - M_dec - (1-SR.C);

                if(R_dec >= 100) SR.C = 0;
                else SR.C = 1;
            }
            A = bcd_to_hex(R_dec);
            set_value_flags(A);
        } else {
            auto [result, v, borrow] = do_add(A, ~operand, SR.C);

            A = result;
            set_value_flags(A);
            SR.C = borrow;
            SR.V = v;
        } break;

        // Logic

        case Operation::And: 
        // fmt::print("and - A=0x{:02x}, M=0x{:02x}\n", A, operand);
        A &= operand;
        set_value_flags(A);
        break;
        case Operation::ExclusiveOr:
        A ^= operand;
        set_value_flags(A);
        break;
        case Operation::Or:
        A |= operand;
        set_value_flags(A);
        break;

        case Operation::ShiftLeft:
        SR.C = ((operand & (1 << 7)) != 0);
        set_value_flags(operand << 1);
        set_operand(operand << 1);
        break;
        case Operation::ShiftRight:
        SR.C = operand & 0x01;
        SR.N = 0;
        set_value_flags(operand >> 1);
        set_operand(operand >> 1);
        break;

        case Operation::RotateLeft: {
            uint8_t rot = operand & (1 << 7);
            set_value_flags(operand << 1 | SR.C);
            set_operand(operand << 1 | SR.C);
            SR.C = (rot != 0);
        } break;
        case Operation::RotateRight: {
            uint8_t rot = operand & 01;
            set_value_flags(operand >> 1 | (SR.C << 7));
            set_operand(operand >> 1 | (SR.C << 7));
            SR.C = rot;
        } break;

        case Operation::ClearCarry:
        SR.C = 0;
        break;
        case Operation::ClearDecimal:
        SR.D = 0;
        break;
        case Operation::ClearInterruptDisable:
        SR.I = 0;
        break;
        case Operation::ClearOverflow:
        SR.V = 0;
        break;

        case Operation::SetCarry:
        SR.C = 1;
        break;
        case Operation::SetDecimal:
        SR.D = 1;
        break;
        case Operation::SetInterruptDisable:
        SR.I = 1;
        break;

        // comparisons

        case Operation::Compare:
        SR.carry = (operand <= A);
        set_value_flags(A - operand);
        // fmt::print("cmp - A=0x{:02x} M=0x{:02x}\n", A, operand);
        break;
        case Operation::CompareX:
        SR.carry = (operand <= X);
        set_value_flags(X - operand);
        break;
        case Operation::CompareY:
        SR.carry = (operand <= Y);
        set_value_flags(Y - operand);
        break;

        // conditional jumps

        case Operation::BranchCarryClear:
        if(SR.C == 0) {
            PC = jump_addr;
        }
        break;
        case Operation::BranchCarrySet:
        if(SR.C == 1) {
            PC = jump_addr;
        }
        break;

        case Operation::BranchOverflowClear:
        if(SR.V == 0) {
            PC = jump_addr;
        }
        break;
        case Operation::BranchOverflowSet:
        if(SR.V == 1) {
            PC = jump_addr;
        }
        break;

        case Operation::BranchPlus:
        if(SR.N == 0) {
            PC = jump_addr;
        }
        break;
        case Operation::BranchMinus:
        if(SR.N == 1) {
            PC = jump_addr;
        }
        break;

        case Operation::BranchEqual:
        if(SR.Z == 1) {
            PC = jump_addr;
        }
        break;
        case Operation::BranchNotEqual:
        if(SR.Z == 0) {
            PC = jump_addr;
        }
        break;

        // unconditional jumps

        case Operation::Jump:
        PC = jump_addr;
        break;

        case Operation::JumpSubroutine: {
            // return address is the old PC + 2
            // which can bne assumed to = new PC - 1
            // the address is incremented on the return instruction
            // (don't know why 6502 behaves this way)
            uint16_t return_addr = PC-1;
            uint8_t hi = (return_addr & 0xFF00) >> 8;
            uint8_t lo = (return_addr & 0x00FF);

            uint16_t hi_addr = 0x0100 | static_cast<uint16_t>(SP);
            memory.write(hi_addr, hi);
            SP--;
            uint16_t lo_addr = 0x0100 | static_cast<uint16_t>(SP);
            memory.write(lo_addr, lo);
            SP--;

            PC = jump_addr;
        } break;

        case Operation::SubroutineReturn: {
            SP++;
            uint16_t return_addr_addr = 0x0100 | static_cast<uint16_t>(SP);
            uint16_t return_addr = memory.read_word(return_addr_addr);
            SP++;
            PC = return_addr + 1;
        } break;

        case Operation::Break: {
            // The return address pushed to the stack is
            // PC+2, providing an extra byte of spacing for a break mark
            // (identifying a reason for the break.)
            uint16_t return_addr = PC+1;
            uint8_t hi = (return_addr & 0xFF00) >> 8;
            uint8_t lo = (return_addr & 0x00FF);

            uint16_t hi_addr = 0x0100 | static_cast<uint16_t>(SP);
            memory.write(hi_addr, hi);
            SP--;
            uint16_t lo_addr = 0x0100 | static_cast<uint16_t>(SP);
            memory.write(lo_addr, lo);
            SP--;

            uint16_t sr_addr = 0x0100 | static_cast<uint16_t>(SP);
            Status copy { SR.value };
            copy.break_ = 1;
            copy._ignored = 1; // this is also set while pushing
            memory.write(sr_addr, copy.value);
            SP--;

            SR.disable_interrupts = 1;

            uint16_t irq_handler = memory.read_word(0xFFFE);
            PC = irq_handler;
        } break;

        case Operation::InterruptReturn: {
            SP++;
            uint16_t sr_addr = 0x0100 | static_cast<uint16_t>(SP);
            Status copy { memory.read_byte(sr_addr) };
            copy.break_ = 0;
            copy._ignored = 0;
            SR.value = copy.value;

            SP++;
            uint16_t return_addr_addr = 0x0100 | static_cast<uint16_t>(SP);
            uint16_t return_addr = memory.read_word(return_addr_addr);
            SP++;

            PC = return_addr;
        } break;

        case Operation::BitTest: {
            SR.N = (operand & (1 << 7)) != 0;
            SR.V = (operand & (1 << 6)) != 0;
            SR.Z = (operand & A) == 0;
        } break;

        case Operation::NoOperation:
        break;

        default:
        fmt::print(stderr, "Unhandled operation, opcode=0x{:02x}, PC=0x{:04x}\n", instr.opcode(), PC);
        halted = true;
        break;
    }

    return cycles;
}

int CPU::execute(int target_cycles) {
    int actual_cycles = 0;
    while (target_cycles > actual_cycles) {
        actual_cycles += execute_next();
        if(breakpoints.find(PC) != breakpoints.end()) break;
        if(halted) break;
    }
    return actual_cycles;
}

std::unordered_map<std::string, std::string> CPU::registers_info() const {
    std::string a = fmt::format("0x{:02x}", A);
    std::string x = fmt::format("0x{:02x}", X);
    std::string y = fmt::format("0x{:02x}", Y);

    std::string sp = fmt::format("0x{:02x}", SP);
    std::string sr = fmt::format("0x{:02x}", SR.value);

    std::string pc = fmt::format("0x{:04x}", PC);

    return {
        { "A", a },   { "X", x },   { "Y", y },
        { "SP", sp }, { "SR", sr }, { "PC", pc }
    };
}

void RAM::load_image(const uint8_t* bin, size_t bytes) {
    assert(this->bytes.size() >= bytes);
    for(size_t i=0; i<bytes; i++) this->bytes[i] = bin[i];
}

std::tuple<uint16_t, std::vector<uint8_t>> RAM::look_around(uint16_t addr) const {
    uint16_t start_addr = addr & 0xFFF0;
    uint16_t end_addr = start_addr + 0x60;
    if(end_addr > bytes.size()) end_addr = bytes.size();

    std::vector<uint8_t> res;
    // res.resize(end_addr-start_addr);
    for(int i=start_addr; i<end_addr; i++) res.push_back(bytes[i]);

    return { start_addr, res };
}

void CPU::skip_next() {
    Instruction instr = fetch_opcode(PC);
    PC += instr.length();
}

int CPU::step(int num_steps) {
    int actual_cycles = 0;
    bool did_first = false;
    for (int i=0; i<num_steps; i++) {
        actual_cycles += execute_next();
        if(halted) break;
        if(debug_interrupted) {
            debug_interrupted = false;
            break;
        }

        if(!did_first) {
            did_first = true;
            continue;
        }

        if(breakpoints.find(PC) != breakpoints.end()) {
            fmt::print("Breakpoint hit at 0x{:04x}\n", PC);
            break;
        }
    }
    return actual_cycles;
}

int CPU::cont() {
    int actual_cycles = 0;
    bool did_first = false;
    for (;;) {
        actual_cycles += execute_next();
        if(halted) break;
        if(debug_interrupted) {
            debug_interrupted = false;
            break;
        }

        if(!did_first) {
            did_first = true;
            continue;
        }

        if(breakpoints.find(PC) != breakpoints.end()) {
            fmt::print("Breakpoint hit at 0x{:04x}\n", PC);
            break;
        }
    }
    return actual_cycles;
}

void CPU::add_breakpoint(uint16_t addr) {
    breakpoints.insert(addr);
}

void CPU::pause_execution() {
    debug_interrupted = true;
}