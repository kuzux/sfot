#include "opcodes.h"

std::array<OpcodeInfo, 256> opcodes = {{
    // generated via the invoking python script like python3 opcodes.py > opcodes.cpp.gen
    #include "opcodes.cpp.gen"
}};