#include "pch.h"
#include "computer.h"
#include "lst_file.h"

#include <signal.h>
#include <stdlib.h>
#include <editline/readline.h>

using namespace std;

// This is accessed concurrently by a signal handler
static volatile sig_atomic_t cpu_running = 0;
// This should probably be protected by something, only for use in the signal handler
static CPU* sig_cpu = NULL;

class Disassembler {
public:
    Disassembler(const RAM& ram) : m_ram(ram) { }
    class Instruction {
    public:
        // this should be only accessible within Disassembler, really
        Instruction(::Instruction base, uint16_t addr, uint8_t hi, uint8_t lo) :
            m_base(base), m_addr(addr), m_lo(lo), m_hi(hi) { }

        uint8_t opcode() const { return m_base.opcode(); }
        uint8_t cycles() const { return m_base.cycles(); }
        uint8_t length() const { return m_base.length(); }

        uint16_t address() const { return m_addr; }

        std::string_view mnemonic() const { return m_base.mnemonic(); }
        Operation operation() const { return m_base.operation(); }
        AddressingMode addressing_mode() const { return m_base.addressing_mode(); }

        uint8_t byte_operand() const { return m_lo; }
        uint16_t word_operand() const { return (m_hi << 8) | m_lo; }
    private:
        ::Instruction m_base;

        uint16_t m_addr;

        // only m_lo is set in case of a 1-byte operand
        uint8_t m_lo;
        uint8_t m_hi;
    };

    vector<Instruction> disassemble(uint16_t start, uint16_t end) const {
        vector<Instruction> disassembly;

        // in case of wraparound
        if(end < start) end = m_ram.size();

        uint16_t pos = start;
        while(pos < end && pos < m_ram.size()) {
            uint8_t opcode = m_ram.read_byte(pos);
            ::Instruction base = ::Instruction::from_opcode(opcode);

            uint8_t lo = 0x00, hi = 0x00;
            if(base.length() >= 2) {
                if(pos+1 >= m_ram.size()) break;
                lo = m_ram.read_byte(pos+1);
            }

            if(base.length() >= 3) {
                if(pos+2 >= m_ram.size()) break;
                lo = m_ram.read_byte(pos+2);
            }

            disassembly.push_back(Instruction(base, pos, hi, lo));

            pos += base.length();
        }
        return disassembly;
    }
private:
    const RAM& m_ram;
};

static void handle_sigint(int _sig) {
    (void)_sig;
    if(!cpu_running) {
        exit(1);
    }
    if(!sig_cpu) return;

    sig_cpu->pause_execution();
    cpu_running = 0;
}

void print_usage() {
    fmt::print(stderr, "Usage: debugger FILENAME\n");
}

void print_help() {
    // [(cmd, explanation)]
    vector<pair<string, string>> cmds_help = {
        { "step", "execute the instruction at PC" },
        { "skip", "skip over the next instruction" },
        { "skip NUM", "skip over NUM instructions" },
        { "break ADDR", "add breakpoint at address ADDR" },
        { "continue", "run the program until the next breakpoint" },
        { "registers", "print register values" },
        { "memory ADDR", "print contents of the memory around ADDR" },
        { "memory ADDR BYTE", "put the value BYTE into memory location ADDR" },
        { "list", "print out the program listing around current PC" },
        { "disassemble", "print out the program disassembly from current PC" },
        { "help", "display this message" },
        { "quit", "quit the debugger" }
    };

    int max_cmd_len = 0;
    for(const auto& p : cmds_help)
        if((int)p.first.length() > max_cmd_len) max_cmd_len = p.first.length();

    int first_column_end = max_cmd_len + 3;

    fmt::print(fmt::emphasis::bold, "Commands:\n");
    for(const auto& p : cmds_help) {
        int num_spaces = first_column_end - p.first.length();
        for(int i=0; i<num_spaces; i++) fmt::print(" ");
        fmt::print("{}  {}\n", p.first, p.second);
    }
}

// returns false if file does not exist
bool load_binary(RAM& ram, const string& filename) {
    ifstream bin(filename, std::ios::binary);
    if(!bin.good()) return false;

    uint8_t buf[65536];
    bin.read((char*)buf, 65536);
    ram.load_image(buf, 65536);

    bin.close();

    return true;
}

vector<string> split_line(string line) {
    vector<string> res;
    stringstream s(line);
    while(!s.eof()) {
        string word; s >> word;
        res.push_back(word);
    }

    return res;
}

int parse_number(string str) {
    int base = 0;
    string repr;

    // https://stackoverflow.com/a/40441240 using rfind for starts with
    if(str.rfind("0x", 0) == 0) {
        base = 16;
        repr = str.substr(2);
    } else if(str.rfind("0d", 0) == 0) {
        base = 16;
        repr = str.substr(2);
    } else {
        base = 16; // TODO: Add decimal/hexadecimal mode
        repr = str;
    }

    return stoi(repr, nullptr, base);
}

void peek_memory(const RAM& ram, uint16_t target) {
    auto [addr, bytes] = ram.look_around(target);
    for(size_t i=0; i<bytes.size(); i++) {
        if(i%8 == 0) {
            fmt::print("0x{:04x} |\t", addr+i);
        }

        if(addr+i == target)
            fmt::print(fmt::emphasis::bold | fg(fmt::color::red), "{:02x}", bytes[i]);
        else
            fmt::print("{:02x}", bytes[i]);

        if(i%8 == 7) fmt::print("\n");
        else if(i%4 == 3) fmt::print("\t");
        else fmt::print(" ");
    }
}

void poke_memory(RAM& ram, uint16_t addr, uint8_t val) {
    ram.write(addr, val);
}

void print_listing(const LstFile& lst_file, uint16_t target) {
    auto listing = lst_file.get_listing(target);

    for(const auto& [addr, line] : listing) {
        fmt::print("0x{:04x}: \t", addr);
        if(addr == target) {
            fmt::print(fmt::emphasis::bold | fg(fmt::color::red), "> ");
        } else {
            fmt::print("  ");
        }

        fmt::print("{}\n", line);
    }
}

void print_disassembly(const Disassembler& disassembler, uint16_t start, uint16_t end) {
    auto disassembly = disassembler.disassemble(start, end);

    for(const auto& ins : disassembly) {
        fmt::print("0x{:04x}: \t", ins.address());

        if(ins.address() == start) {
            fmt::print(fmt::emphasis::bold | fg(fmt::color::red), "> ");
        } else {
            fmt::print("  ");
        }

        // TODO: Print raw machine code as well

        fmt::print("{} ", ins.mnemonic());

        switch(ins.addressing_mode()) {
        case AddressingMode::Implied: break;
        case AddressingMode::Accumulator: fmt::print("a"); break;
        case AddressingMode::Immediate: fmt::print("#${:02x}", ins.byte_operand()); break;
        case AddressingMode::ZeroPage: fmt::print("${:02x}", ins.byte_operand()); break;
        case AddressingMode::ZeroPageX: fmt::print("${:02x}, x", ins.byte_operand()); break;
        case AddressingMode::ZeroPageY: fmt::print("${:02x}, y", ins.byte_operand()); break;
        case AddressingMode::Absolute: fmt::print("${:04x}", ins.word_operand()); break;
        case AddressingMode::AbsoluteX: fmt::print("${:04x}, x", ins.word_operand()); break;
        case AddressingMode::AbsoluteY: fmt::print("${:04x}, y", ins.word_operand()); break;
        case AddressingMode::Indirect: fmt::print("(${:04x})", ins.word_operand()); break;
        case AddressingMode::XIndirect: fmt::print("(${:02x}, x)", ins.byte_operand()); break;
        case AddressingMode::IndirectY: fmt::print("(${:02x}), y", ins.byte_operand()); break;
        case AddressingMode::Relative: fmt::print("${:02d}", (int8_t)ins.byte_operand()); break;
        }

        fmt::print("\n");
    }
}

int main(int argc, const char** argv) {
    RAM ram(65536);
    CPU cpu(ram);

    // TODO: Similar to this, there should be a debugger class for cpu related things
    // Plus maybe for ram::look_around
    Disassembler disassembler(ram);

    if(argc <= 1) {
        print_usage();
        return 1;
    }

    std::string filename { argv[1] };

    if(!load_binary(ram, filename)) {
        fmt::print(stderr, "{}: File not found (or could not be opened)\n", argv[1]);
        return 1;
    }

    string lst_filename = filename.substr(0, filename.find_last_of('.')) + ".lst";
    LstFile lst_file(lst_filename);

    if(lst_file.loaded()) {
        fmt::print("Loaded listing file {}\n", lst_filename);
    }

    sig_cpu = &cpu;
    signal(SIGINT, handle_sigint);

    rl_initialize();

    cpu.reset();

    char* line;
    while((line = readline("> ")) != NULL) {
        add_history(line);
        auto words = split_line(line);

        if(words[0] == "s" || words[0] == "step") {
            int steps = 1;
            if(words.size() >= 2) {
                steps = parse_number(words[1]);
                fmt::print("Stepping {} times\n", steps);
            } else {
                fmt::print("Stepping once\n");
            }
            cpu_running = 1;
            int cycles = cpu.step(steps);
            cpu_running = 0;
            fmt::print("CPU ran for {} cycles\n", cycles);
        } else if(words[0] == "k" || words[0] == "skip") {
            if(words.size() == 1) {
                fmt::print("Skipping next instruction\n");
                cpu.skip_next();
            } else {
                int count = parse_number(words[1]);
                fmt::print("Skipping next {} instructions\n", count);
                for(int i=0; i<count; i++) cpu.skip_next();
            }
        } else if(words[0] == "c" || words[0] == "continue" || words[0] == "run") {
            cpu_running = 1;
            int cycles = cpu.cont();
            cpu_running = 0;
            fmt::print("CPU ran for {} cycles\n", cycles);
        } else if(words[0] == "r" || words[0] == "registers") {
            auto info = cpu.registers_info();
            for(auto [k, v] : info)
                fmt::print("{}\t{}\n", k, v);
        } else if(words[0] == "m" || words[0] == "mem" || words[0] == "memory") {
            if(words.size() == 1) {
                fmt::print("No address given\n");
                continue;
            }
            if(words.size() == 2) {
                uint16_t addr = parse_number(words[1]);
                peek_memory(ram, addr);
                continue;
            }
            if(words.size() == 3) {
                uint16_t addr = parse_number(words[1]);
                uint16_t val = parse_number(words[2]);
                poke_memory(ram, addr, val);
                continue;
            }
        } else if(words[0] == "l" || words[0] == "list") {
            if(!lst_file.loaded()) {
                fmt::print("No listing file loaded\n");
                continue;
            }

            print_listing(lst_file, cpu.PC);
        } else if(words[0] == "b" || words[0] == "break") {
            uint16_t addr = parse_number(words[1]);
            fmt::print("Adding breakpoint at address {0:d} = 0x{0:04x}\n", addr);
            cpu.add_breakpoint(addr);
        } else if(words[0] == "d" || words[0] == "disassemble") {
            print_disassembly(disassembler, cpu.PC, cpu.PC+50);
        } else if(words[0] == "h" || words[0] == "help") {
            print_help();
        } else if(words[0] == "q" || words[0] == "quit") {
            fmt::print("Bye\n");
            break;
        } else {
            fmt::print("Unrecognized command {}\n", words[0]);
        }
    }

    return 0;
}
