RELEASE?=0

PROG=debugger
SRCS=$(wildcard *.cpp)
OBJS=$(SRCS:.cpp=.o)
DEPFILES=$(SRCS:.cpp=.d)

CXXFLAGS=-fsanitize=address -Wall -Wextra -std=c++17 -MMD
LDFLAGS=-fsanitize=address -ledit -lfmt

ifeq ($(RELEASE), 1)
	CXXFLAGS+=-O2
else
	CXXFLAGS+=-g
endif

all: $(PROG)

$(PROG): pch.h.gch $(OBJS)
	$(CXX) $(OBJS) -o $@ $(LDFLAGS)

pch.h.gch: pch.h
	$(CXX) -x c++-header -c $< -o $@ $(CXXFLAGS)

%.o: %.cpp
	$(CXX) -include pch.h -c $< -o $@ $(CXXFLAGS)

clean:
	rm -f $(PROG) $(OBJS) $(DEPFILES) pch.h.gch

-include ${DEPFILES}