#pragma once

#include "pch.h"
class LstFile {
public:
    LstFile(std::string filename);
    bool loaded() const { return m_loaded; }

    // gets program listing from around the given address
    // returns a list of (addr, line) tuples
    std::vector<std::tuple<uint16_t, std::string>> get_listing(uint16_t addr) const;
private:
    struct LstLine {
        uint16_t address;
        std::string line;
        bool relocatable;
    };

    bool m_loaded = false;
    std::vector<LstLine> m_lines;
};