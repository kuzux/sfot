#pragma once

enum class AddressingMode {
    Implied,
    Immediate,
    Accumulator,
    Absolute,
    AbsoluteX,
    AbsoluteY,
    Relative,
    Indirect,
    XIndirect,
    IndirectY,
    ZeroPage,
    ZeroPageX,
    ZeroPageY
};