#include <stdint.h>

#include <vector>
#include <functional>
#include <array>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <charconv>

#include <fstream>
#include <sstream>
#include <iostream>

#include <fmt/core.h>
#include <fmt/color.h>
