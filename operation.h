#pragma once

enum class Operation {
    Invalid,                // For any invalid / non-jamming operation

    Add,                    // ADC
    And,                    // AND
    ShiftLeft,              // ASL
    BranchCarryClear,       // BCC
    BranchCarrySet,         // BCS
    BranchEqual,            // BEQ
    BitTest,                // BIT
    BranchMinus,            // BMI
    BranchNotEqual,         // BNE
    BranchPlus,             // BPL
    Break,                  // BRK
    BranchOverflowClear,    // BVC
    BranchOverflowSet,      // BVS
    ClearCarry,             // CLC
    ClearDecimal,           // CLD
    ClearInterruptDisable,  // CLI
    ClearOverflow,          // CLV
    Compare,                // CMP
    CompareX,               // CPX
    CompareY,               // CPY
    Decrement,              // DEC
    DecrementX,             // DEX
    DecrementY,             // DEY
    ExclusiveOr,            // EOR
    Increment,              // INC
    IncrementX,             // INX
    IncrementY,             // INY
    Jump,                   // JMP
    JumpSubroutine,         // JSR
    LoadAcc,                // LDA
    LoadX,                  // LDX
    LoadY,                  // LDY
    ShiftRight,             // LSR
    NoOperation,            // NOP
    Or,                     // ORA
    PushAcc,                // PHA
    PushStatus,             // PHP
    PullAcc,                // PLA
    PullStatus,             // PLP
    RotateLeft,             // ROL
    RotateRight,            // ROR
    InterruptReturn,        // RTI
    SubroutineReturn,       // RTS
    Subtract,               // SBC
    SetCarry,               // SEC
    SetDecimal,             // SED
    SetInterruptDisable,    // SEI
    StoreAcc,               // STA
    StoreX,                 // STX
    StoreY,                 // STY
    AccToX,                 // TAX
    AccToY,                 // TAY
    SPToX,                  // TSX
    XToAcc,                 // TXA
    XToSP,                  // TXS
    YToAcc,                 // TYA
    Halt                    // JAM
};
