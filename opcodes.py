import json

file = open('opcodes.txt', 'r')
lines = map(lambda l: l.rstrip(), file.readlines())
file.close()

header_file = open('operation.h', 'r')
header_lines = map(lambda l: l.strip(), header_file.readlines())
header_lines = filter(lambda l: len(l) > 0, header_lines)
header_file.close()

header_lines = list(header_lines)

opener_index = 0
for index, line in enumerate(header_lines):
	if line.endswith('{'):
		opener_index = index+1
		break
closer_index = -1
for index, line in reversed(list(enumerate(header_lines))):
	if line.startswith('}'):
		closer_index = index
		break

header_lines = header_lines[opener_index:closer_index]

def source_to_names(line):
	parts = line.split('//')
	parts[0] = parts[0].strip()
	parts[1] = parts[1].strip()

	if parts[0].endswith(','): parts[0] = parts[0][:-1]

	return { 'mnemonic': parts[1], 'name': parts[0] }

mnemonics = list(map(source_to_names, header_lines))

json_file = open('cycles.json', 'r')
cycles = json.load(json_file)
json_file.close()

addressing_modes = {
	'impl': 'Implied',
	'A': 'Accumulator',
	'#': 'Immediate',
	'zpg': 'ZeroPage',
	'zpg,X': 'ZeroPageX',
	'zpg,Y': 'ZeroPageY',
	'abs': 'Absolute',
	'abs,X': 'AbsoluteX',
	'abs,Y': 'AbsoluteY',
	'ind': 'Indirect',
	'X,ind': 'XIndirect',
	'ind,Y': 'IndirectY',
	'rel': 'Relative'
}

res = ["// /* opcode */ { mnemonic, operation, addressing mode, # of cycles }"]
for index, line in enumerate(lines):
	words = line.split(' ')

	opcode = format(index, '02x').upper()

	mne = words[0]
	am = None
	if mne == "JAM": 
		am = "impl"
		op_cycles = 1
	else: 
		am = words[1]
		op_cycles = -1

	am_name = addressing_modes[am]
	op_name = "Invalid"
	for op in mnemonics:
		if op["mnemonic"] == mne:
			op_name = op["name"]
	for c in cycles:
		if c["opcode"] == opcode:
			op_cycles = c["cycles"]

	res.append(f"/* {opcode} */ {{ \"{mne.lower()}\", Operation::{op_name}, AddressingMode::{am_name}, {op_cycles} }},")

print('\n'.join(res))